function init() {
  // Pieces that are stuck in the middle of the board.
  white_middle_board_pieces = []
  black_middle_board_pieces = []


  // Get global elements.
  board_elem = document.getElementById('myBoard');
  source_column_elem = document.getElementById('source_column');
  destination_column_elem = document.getElementById('destination_column');
  roll_dice_elem = document.getElementById('roll_dice');
  move_elem = document.getElementById('move_button');
  turn_elem = document.getElementById('turn');
  dice_1_elem = document.getElementById('dice1');
  dice_2_elem = document.getElementById('dice2');
  dice_3_elem = document.getElementById('dice3');
  dice_4_elem = document.getElementById('dice4');
  color_piece_elem = document.getElementById('color_piece');
  server_response_msg_elem = document.getElementById('server_response_msg');

  // Setup handlers.
  roll_dice_elem.onclick = roll_dice
  move_elem.onclick = move_piece

  // Get player and game UUID from the cookies.
  console.log("backgammon.js cookies: " + document.cookie);
  player_uuid = find_cookie_value('new_player_uuid')
  console.log("player_uuid cookie: " + player_uuid);
  game_uuid = find_cookie_value('game_uuid')
  console.log("game_uuid cookie: " + game_uuid);

  // Initial board.
  board = null;
  get_board();

  initial_roll();

  poll_interval = setInterval(poll_server, 3000);
}

/**
 * Handles the initial roll when the game is loaded. This removes the need for
 * users to manually press a roll button to determine who goes first. This
 * function gets both the initial turn and the user's piece color (e.g. 
 * either white or black).
 */
function initial_roll() {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", game_uuid + "?player_uuid="+ player_uuid + "&request_type=initial_dice_roll", true);
  xhttp.onload = function(e) {
    if (xhttp.readyState === 4) {
      if (xhttp.status === 200) {
        init_dice_response = xhttp.responseText
        console.log(init_dice_response)
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(init_dice_response,"text/xml");
        your_turn = xmlDoc.getElementsByTagName('your_turn')[0].innerHTML
        if (your_turn == 'true') {
          turn_elem.innerHTML = "Your turn!";
        }
        else {
          turn_elem.innerHTML = "Opponent's turn...";
        }
        player_color = xmlDoc.getElementsByTagName('color_piece')[0].innerHTML
        if (player_color == 'B') {
          color_piece_elem.innerHTML = 'Piece color: Black (B)'
        }
        else {
          color_piece_elem.innerHTML = 'Piece color: White (W)'
        }
      }
    }
  }
  xhttp.send();
}

/**
 * Poll the server for any updates.
 */
function poll_server() {
  var xhttp = new XMLHttpRequest(); 
  xhttp.open("GET", game_uuid + "?player_uuid="+ player_uuid + "&request_type=poll", true);
  xhttp.onload = function(e) {
    if (xhttp.readyState === 4) {
      if (xhttp.status === 200) {
        // Poll for new turn.
        update = xhttp.responseText;
        //console.log(update);
        if (update == 'B' || update == 'W') {
          // Someone won, most likely the opponent.
          if (update == player_color) {
            turn_elem.innerHTML = "YOU WIN!!!";
          }
          else {
            turn_elem.innerHTML = "You lose.";
          }
          get_board();
          clearInterval(poll_interval); // Stop polling the server.
        }
        if (turn_elem.innerHTML != update) {
          if (update == 'Opponent\'s turn...') {
            turn_elem.innerHTML = update;
            get_board();
          }
          else if (update == 'Your turn!') {
            turn_elem.innerHTML = update;
            get_board();
          }
        }
      }
    }
  }
  xhttp.send();
}

/**
 * Gets the current game board in JSON format from the server. The first
 * 24 arrays are the rows. Then the following 4 arrays are the pieces 
 * in the middle column and off of the board.
 */
function get_board() {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", game_uuid+"?player_uuid="+player_uuid+"&request_type=get_board", true);
  console.log("refreshing board");
  xhttp.onload = function(e) {
    if (xhttp.readyState === 4) {
      if (xhttp.status === 200) {
        json = xhttp.responseText;
        json = json.replace(/\'/g, '"') // Replace all single quotes with double quotes.
        console.log(json);
        board = JSON.parse(json);
        console.log(board)
        board_elem.innerHTML = print_board();
      }
    }
  }
  xhttp.send();
}

/**
 * Request the dice in play from the server and update the UI.
 */
function roll_dice() {
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", game_uuid+"?player_uuid="+player_uuid+"&request_type=roll_dice", true);
  xhttp.onload = function(e) {
    if (xhttp.readyState === 4) {
      if (xhttp.status === 200) {
        dice = xhttp.responseText;
        console.log("roll_dice return: " + dice);
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(dice,"text/xml");
        if (xmlDoc.getElementsByTagName("dice_1").length == 0) {
          // Error message was send as response. Show user the response.
          server_response_msg_elem.innerHTML = dice;
          return;
        }
        dice_1_elem.innerHTML = xmlDoc.getElementsByTagName("dice_1")[0].innerHTML;
        dice_2_elem.innerHTML = xmlDoc.getElementsByTagName("dice_2")[0].innerHTML;
        // Don't update dice 3 and 4 if they're empty from not rolling matching numbers.
        if (xmlDoc.getElementsByTagName("dice_3")[0].innerHTML == "") {
          return;
        }
        dice_3_elem.innerHTML = xmlDoc.getElementsByTagName("dice_3")[0].innerHTML;
        dice_4_elem.innerHTML = xmlDoc.getElementsByTagName("dice_4")[0].innerHTML;
      }
    }
  }
  xhttp.send();
}

/**
 * Attempt to move a piece from source row to destination row.
 */
function move_piece() {
  src_column = document.getElementById('source_column').value;
  dst_column = document.getElementById('destination_column').value;
  console.log("src_column="+src_column);
  console.log("dst_column="+dst_column);
  if (src_column.length == 0 || dst_column.length == 0) {
    server_response_msg_elem.innerHTML = "Must have source and destination column filled in before moving!";
    return;
  }
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", game_uuid+"?player_uuid="+player_uuid+
  "&request_type=move_piece&src_column="+src_column+"&dst_column="+
  dst_column, true);
  xhttp.onload = function(e) {
    if (xhttp.readyState === 4) {
      if (xhttp.status === 200) {
        response = xhttp.responseText
        console.log(this.response)
        if (this.response == '1') {
          console.log("Move accepted!");
          // Move accepted.
          remove_dice_from_ui();
          get_board();
          server_response_msg_elem.innerHTML = "";
        }
        else if (response == '2') {
          // Next player's turn. Toggle the UI turn tracker.
          if (turn_elem.innerHTML == "Opponent's turn") {
            turn_elem.innerHTML = 'Your turn';
          }
          else {
            turn_elem.innerHTML = "Opponent's turn";
          }
          remove_dice_from_ui();
          get_board();
          server_response_msg_elem.innerHTML = "";
        }
        else if (this.response == 'B' || this.response == 'W') {
          if (this.response == player_color) {
            turn_elem.innerHTML = 'YOU WIN!!!';
          }
          else {
            turn_elem.innerHTML = 'You lose.';
          }
          server_response_msg_elem.innerHTML = "";
        }
        else {
          server_response_msg_elem.innerHTML = "Invalid move.";
        }
      }
      if (xhttp.status == 400) {
        server_response_msg_elem.innerHTML = 'Invalid input syntax!';
      }
    }
  }
  xhttp.send();
}

/**
 * Called on successful move request. Find the distance between the source and 
 * destination row and remove the dice of that value from the UI.
 */
function remove_dice_from_ui() {
  src_column = Number(src_column);
  dst_column = Number(dst_column);
  if (src_column > dst_column) {
    distance = src_column - dst_column;
  }
  else {
    distance = dst_column - src_column;
  }
  distance = distance.toString();
  // Remove the dice of distance from the UI.
  if (dice_1_elem.innerHTML == distance) {
    dice_1_elem.innerHTML = '';
    return;
  }
  else if (dice_2_elem.innerHTML == distance) {
    dice_2_elem.innerHTML = '';
    return;
  }
  else if (dice_3_elem.innerHTML == distance) {
    dice_3_elem.innerHTML = '';
    return;
  }
  else if (dice_4_elem.innerHTML == distance) {
    dice_4_elem.innerHTML = '';
    return;
  }
}

/**
 * Finds the cookie value of the given key.
 * @param {*} cookie_key the cookie key
 * Return: the value associated with the cookie key.
 */
function find_cookie_value(cookie_key) {
  cookies = document.cookie.split(" ")
  for (i in cookies) {
    values = cookies[i].split("=");
    if (values[0] == cookie_key) {
      console.log("find_cookie_value returning " + values[1]);
      return values[1].replace(';', '');
    }
  }
}

/**
 * Returns a formatted HTML representation of the game board.
 */
function print_board() {
  return "Black pieces in the middle of the board (Row -2): " + board[24] + "<br>" +
  "White pieces in the middle of the board (Row -2): " + board[25] + "<br><br>" +
  "Black pieces off the board (Row -1): " + board[26] + "<br>" +
  "Row 00 | " + board[0] + "<br>" +
  "Row 01 | " + board[1] + "<br>" +
  "Row 02 | " + board[2] + "<br>" +
  "Row 03 | " + board[3] + "<br>" +
  "Row 04 | " + board[4] + "<br>" +
  "Row 05 | " + board[5] + "<br>" +
  "Row 06 | " + board[6] + "<br>" +
  "Row 07 | " + board[7] + "<br>" +
  "Row 08 | " + board[8] + "<br>" +
  "Row 09 | " + board[9] + "<br>" +
  "Row 10 | " + board[10] + "<br>" +
  "Row 11 | " + board[11] + "<br>" +
  "Row 12 | " + board[12] + "<br>" +
  "Row 13 | " + board[13] + "<br>" +
  "Row 14 | " + board[14] + "<br>" +
  "Row 15 | " + board[15] + "<br>" +
  "Row 16 | " + board[16] + "<br>" +
  "Row 17 | " + board[17] + "<br>" +
  "Row 18 | " + board[18] + "<br>" +
  "Row 19 | " + board[19] + "<br>" +
  "Row 20 | " + board[20] + "<br>" +
  "Row 21 | " + board[21] + "<br>" +
  "Row 22 | " + board[22] + "<br>" +
  "Row 23 | " + board[23] + "<br>" +
  "White pieces off the board (Row 24): " + board[27] + "<br>"
}