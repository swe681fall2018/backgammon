from secrets import randbelow
from common.logger import logger
from game.TurnTimer import TurnTimer
from threading import Event


class Game:

    def __init__(self):
        self.BLACK_PIECE = "B"
        self.WHITE_PIECE = "W"
        self.board = [[] for _ in range(24)]

        # The middle part of the board that holds pieces that the opponent
        # landed on.
        self.white_middle_board_pieces = []
        self.black_middle_board_pieces = []

        # The pieces that have crossed the finish line and are off the
        # board.
        self.white_off_board_pieces = []
        self.black_off_board_pieces = []

        self.init_board()

        # Placeholders for the players. Player 1 is always black and Player 2
        # is always white.
        self.PLAYER_1 = self.BLACK_PIECE
        self.PLAYER_2 = self.WHITE_PIECE

        # Determines which player's turn it is.
        self.current_players_turn = None

        # The column indexes for moving a piece off the board.
        self.OFF_BLACK = -1
        self.OFF_WHITE = 24
        
        self.MIDDLE_COLUMN = -2

        self.player_who_won = None

        # Start the turn thread. If the user hasn't responded in 30 seconds,
        # they miss their turn.
        self.turn_event = Event()
        self.turn_timer = TurnTimer(self, self.turn_event)
        self.turn_timer.start()

    def init_board(self):
        """ Places the pieces in their default location at the start of a
        game.
        """
        self.board[0]

        # Initialize the black pieces on the board.
        self.board[5].extend([self.BLACK_PIECE for _ in range(5)])
        self.board[7].extend([self.BLACK_PIECE for _ in range(3)])
        self.board[12].extend([self.BLACK_PIECE for _ in range(5)])
        self.board[23].extend([self.BLACK_PIECE for _ in range(2)])

        # Initialize the white pieces on the board.
        self.board[0].extend([self.WHITE_PIECE for _ in range(2)])
        self.board[11].extend([self.WHITE_PIECE for _ in range(5)])
        self.board[16].extend([self.WHITE_PIECE for _ in range(3)])
        self.board[18].extend([self.WHITE_PIECE for _ in range(5)])

        print(self.__str__())

        # A list of the dice that have been roled and unused for the current
        # user's turn.
        self.dice_in_play = []

    def determine_who_goes_first(self):
        """ Determine which player goes first when a new game starts.

        Return: the PLAYER_{1,2} constant that goes first.
        """
        while True:
            roll_1 = randbelow(6) + 1
            logger.info("Game.determine_who_goes_first roll 1: " + str(roll_1))
            roll_2 = randbelow(6) + 1
            logger.info("Game.determine_who_goes_first roll 2: " + str(roll_2))
            if roll_1 == roll_2:
                logger.info("Game.determine_who_goes_first tie. Rerolling.")
                continue
            elif roll_1 > roll_2:
                self.current_players_turn = self.PLAYER_1
                logger.info("Game.determine_who_goes_first PLAYER_1 goes first")
                return self.PLAYER_1
            else:
                self.current_players_turn = self.PLAYER_2
                logger.info("Game.determine_who_goes_first PLAYER_2 goes first")
                return self.PLAYER_2

    def roll_dice(self):
        """ Rolls 2d6 and adds the dice numbers to dice_in_play.

        Return:
        * True - successful dice roll
        * False - did not roll dice
        """
        if len(self.dice_in_play) != 0:
            logger.error(
                "Game.roll_dice can't roll when there are still "
                "dice left {}.".format(self.dice_in_play)
                )
            return False
        roll_1 = randbelow(6) + 1
        logger.info("Game.roll_dice roll 1: " + str(roll_1))
        roll_2 = randbelow(6) + 1
        logger.info("Game.roll_dice roll 2: " + str(roll_2))

        # Matching dices are given twice as many dice.
        if roll_1 == roll_2:
            self.dice_in_play.extend([roll_1]*2)
            self.dice_in_play.extend([roll_2]*2)
        else:
            self.dice_in_play.extend([roll_1, roll_2])
        return True

    def next_turn(self):
        """ Moves the game to the next player's turn.
        """
        if self.current_players_turn == self.PLAYER_1:
            logger.info("Game.next_turn PLAYER_1 to PLAYER_2")
            self.current_players_turn = self.PLAYER_2
        else:
            logger.info("Game.next_turn PLAYER_2 to PLAYER_1")
            self.current_players_turn = self.PLAYER_1

    def valid_column_index(self, column_index):
        if column_index >= 0 and column_index <= 23:
            return True
        else:
            logger.error(
                "Game.valid_column_index invalid index: " +
                str(column_index)
                )
            return False

    def validate_source_column(self, src_column):
        """ Verify that the source piece is the same color of the user who has
        the current turn.
        """
        if self.current_players_turn in self.board[src_column]:
            return True
        else:
            logger.error(
                "Game.validate_source_column invalid source piece "
                "color. Found {} for current player with color {}."
                .format(self.board[src_column], self.current_players_turn)
                )
            return False

    def move(self, src_column, dst_column):
        """ Attempt to move a piece from the source column param to the
        destination column param.

        Params:
        * src_column - the column [0-23] of the piece that the user wants to
        move.
        * dst_column - the column [0-23] of the piece that the user wants to
        move the piece to.

        Assumptions:
        * The GameSession has already verified that the user who is making
        the request is the user who has the current turn.
        * The dice have already been rolled for the current user's turn.

        Return (I know that returning ints is hacky C-like code but I'm lazy):
        * None - move rejected
        * 1 - move accepted
        * 2 - next players turn
        * 3 - a player won the game
        """
        # Moving pieces off the board is handled elsewhere.
        if dst_column == self.OFF_BLACK or dst_column == self.OFF_WHITE:
            return self.move_piece_off_board(src_column, dst_column)

        # Moving piece from middle of the board is handled elsewhere.
        if src_column == self.MIDDLE_COLUMN:
            return self.move_piece_from_middle_column(src_column, dst_column)
        
        # Must move piece off of the middle column first.
        middle_column = None
        if self.current_players_turn == self.PLAYER_1:
            middle_column = self.black_middle_board_pieces
        else:
            middle_column = self.white_middle_board_pieces
        if len(middle_column) != 0:
            logger.error(
                "Game.move player must move piece from middle column. Source "
                "column is {} and middle column is {}.".format(
                    src_column, middle_column
                ))
            return None

        # Sanitize column values.
        if not self.valid_column_index(src_column) or \
        not self.valid_column_index(dst_column):
            return None

        if self.validate_source_column(src_column) is False:
            return None

        # Verify that the destination column is the same color, empty,
        # off the board, or contains a single opponent piece that will get
        # removed from the board.
        if self.current_players_turn in self.board[dst_column]:
            pass
        elif len(self.board[dst_column]) == 0:
            pass
        elif self.current_players_turn not in self.board[dst_column] and \
        len(self.board[dst_column]) == 1:
            return self.replace_opponent_piece(src_column, dst_column)
        else:
            logger.error("Game.move invalid destination column. Tried to move"
            " piece from column {} {} to column {} {}.".format(src_column,
            self.board[src_column], dst_column, self.board[dst_column]))
            return None

        # Get the distance of the desired move and verify that it's a valid
        # move according to rolled dice.
        move_distance = self.get_distance(src_column, dst_column)
        if move_distance is None:
            return None
        else:
            self.dice_in_play.remove(move_distance)

        moving_piece = self.board[src_column].pop()
        self.board[dst_column].append(moving_piece)
        logger.info(
            "Game.move player {} moving piece from column {} to column"
            " {}. {}".format(self.current_players_turn, src_column, dst_column,
            self.__str__())
            )

        return self.check_for_next_players_turn()

    def check_for_next_players_turn(self):
        """ Determine if the user's turn is over.

        Return:
        * 1 - No
        * 2 - Yes
        """
        if len(self.dice_in_play) == 0:
            logger.info("Game.check_for_next_players_turn is changing the turn.")
            self.turn_event.set()
            return 2
        else:
            return 1

    def move_piece_from_middle_column(self, src_column, dst_column):
        """ Move piece from the middle column to the board.

        Return:
        * None - move rejected
        * 1 - move accepted
        * 2 - next players turn
        """
        middle_column = []
        distance = None
        if self.current_players_turn == self.PLAYER_1:
            middle_column = self.black_middle_board_pieces
            distance = 24 - dst_column
        else:
            middle_column = self.white_middle_board_pieces
            distance = dst_column + 1

        # Verify that a piece is in the middle column.
        if len(middle_column) == 0:
            logger.error(
                "Game.move_piece_from_middle_column tried to move piece from "
                "middle column, but middle column is empty."
            )
            return None

        # Verify distance is in current dice in play.
        if distance not in self.dice_in_play:
            logger.error(
                "Game.move_piece_from_middle_column distance of {} not in dice"
                " values {}.".format(distance, self.dice_in_play)
                )
            return None

        # Verify destination column.
        if self.current_players_turn in self.board[dst_column]:
            pass
        elif len(self.board[dst_column]) == 0:
            pass
        elif self.current_players_turn not in self.board[dst_column] and \
        len(self.board[dst_column]) == 1:
            # Replace the opponent's piece in the destination column.
            piece_to_move = middle_column.pop()
            piece_to_replace = self.board[dst_column].pop()
            assert(len(self.board[dst_column]) == 0)
            self.board[dst_column].append(piece_to_move)
            logger.info(
                "Game.move_piece_from_middle_column replacing piece from "
                "column {} {} with piece from middle column.".format(
                    dst_column, self.board[dst_column]
                )
            )
            if piece_to_replace == self.BLACK_PIECE:
                self.black_middle_board_pieces.append(piece_to_replace)
            else:
                self.white_middle_board_pieces.append(piece_to_replace)
            return self.check_for_next_players_turn()
        else:
            logger.error(
                "Game.move_piece_from_middle_column invalid destination "
                "column. Tried to move piece from column {} {} to column "
                "{} {}.".format(
                    src_column, middle_column, dst_column, 
                    self.board[dst_column]
                    )
                )
            return None

        # Place piece.
        piece_to_move = middle_column.pop()
        self.board[dst_column].append(piece_to_move)
        logger.info(
            "Game.move_piece_from_middle_column move piece from middle column "
            "to column {} {}.".format(dst_column, self.board[dst_column])
            )

        # Remove the dice used.
        self.dice_in_play.remove(distance)

        return self.check_for_next_players_turn()

    def move_piece_off_board(self, src_column, dst_column):
        """ Moves a players piece off of the board.

        Return:
        * None - rejected move
        * 1 - move accepted
        * 2 - next players turn
        * 3 - player has won
        """
        # Check if all pieces are in the last 6 columns of the board.
        if self.current_players_turn == self.PLAYER_1:
            for column in range(6, 24):
                if self.BLACK_PIECE in self.board[column]:
                    logger.error(
                        "Game.move_piece_off_board cannot move piece"
                        " from column {} off of P1's board because the board "
                        "still has pieces in columns 6-23!".format(src_column)
                        )
                    return None
        else:
            for column in range(0, 18):
                if self.WHITE_PIECE in self.board[column]:
                    logger.error(
                        "Game.move_piece_off_board cannot move piece"
                        " from column {} off of P2's board because the board "
                        "still has pieces in columns 0-18!".format(src_column)
                        )
                    return None

        # Check if the player has a die that can move the piece off the board.
        if self.current_players_turn == self.PLAYER_1:
            off_board_distance = src_column - dst_column
        else:
            off_board_distance = dst_column - src_column
        # Always use the shortest die roll possible to move the piece off the
        # board.
        self.dice_in_play.sort()
        die_roll_used = 0  # Prevent concurrent modification.
        for die_roll in self.dice_in_play:
            if die_roll >= off_board_distance:
                die_roll_used = die_roll
                break

        if die_roll_used == 0:
            logger.error(
                "Game.move_piece_off_board moving a piece off the "
                "board from column {} requires a roll of {} which is invalid "
                " with dice {}.".format(
                    src_column, off_board_distance, self.dice_in_play
                    )
                )
            return None

        # It's a valid move!
        removed_piece = self.board[src_column].pop()
        if removed_piece == self.WHITE_PIECE:
            self.white_off_board_pieces.append(removed_piece)
        else:
            self.black_off_board_pieces.append(removed_piece)
        self.dice_in_play.remove(die_roll_used)
        logger.info(
            "Game.move_piece_off_board moved piece from column {} off "
            " of the board.".format(src_column)
            )

        # Check if anyone won the game.
        if self.current_players_turn == self.BLACK_PIECE and \
        len(self.black_off_board_pieces) == 15:
            logger.info("Game.move_piece_off_board PLAYER_1 has WON!")
            self.player_who_won = self.PLAYER_1
            return 3
        elif self.current_players_turn == self.WHITE_PIECE and \
        len(self.white_off_board_pieces) == 15:
            logger.info("Game.move_piece_off_board PLAYER_2 has WON!")
            self.player_who_won = self.PLAYER_2
            return 3

        return self.check_for_next_players_turn()

    def replace_opponent_piece(self, src_column, dst_column):
        """ Takes piece at column src_column and replaces the single opponent
        piece at column dst_column. It adds the opponent's piece to the middle
        column of the board.

        Context:
        * src_column contains the current player's piece that will be moved
        * dst_column contains a single opponent piece

        Return:
        * None - rejected move
        * 1 - move accepted
        * 2 - next players turn
        """
        distance = self.get_distance(src_column, dst_column)
        if distance is None:
            return None
        else:
            self.dice_in_play.remove(distance)

        piece_to_move = self.board[src_column].pop()
        piece_to_replace = self.board[dst_column].pop()
        assert(len(self.board[dst_column]) == 0)
        self.board[dst_column].append(piece_to_move)

        if piece_to_replace == self.BLACK_PIECE:
            self.black_middle_board_pieces.append(piece_to_replace)
        else:
            self.white_middle_board_pieces.append(piece_to_replace)

        return self.check_for_next_players_turn()

    def get_distance(self, src_column, dst_column):
        """ Check that the move's distance matches one of the possible dice.
        The distance must be calculated on a per player basis because they
        are advancing pieces in different directions.

        Note: this does not remove the dice with the value distance from the
        dice in play.

        Return:
        * The distance on success.
        * None - if the distance is not in the dice roll.
        """
        move_distance = 0
        if self.current_players_turn == self.PLAYER_1:
            # Advancing pieces from column 23 -> column 0.
            move_distance = src_column - dst_column
        else:
            # Advancing pieces from column 0 -> column 23.
            move_distance = dst_column - src_column
        if move_distance not in self.dice_in_play:
            logger.error(
                "Game.move invalid move distance. Move value {} with "
                "current dice values {}.".format(
                    move_distance, self.dice_in_play
                    )
                )
            return None
        else:
            return move_distance

    def __str__(self):
        return """
        Black middle of board: {}
        White middle of board: {}

        Black pieces off the board: {}
        Row 0: {}
        Row 1: {}
        Row 2: {}
        Row 3: {}
        Row 4: {}
        Row 5: {}
        Row 6: {}
        Row 7: {}
        Row 8: {}
        Row 9: {}
        Row 10: {}
        Row 11: {}
        Row 12: {}
        Row 13: {}
        Row 14: {}
        Row 15: {}
        Row 16: {}
        Row 17: {}
        Row 18: {}
        Row 19: {}
        Row 20: {}
        Row 21: {}
        Row 22: {}
        Row 23: {}
        White pieces off the board: {}
        """.format(
            self.black_middle_board_pieces,
            self.white_middle_board_pieces, self.black_off_board_pieces,
            self.board[0], self.board[1], self.board[2], self.board[3],
            self.board[4], self.board[5], self.board[6], self.board[7],
            self.board[8], self.board[9], self.board[10], self.board[11],
            self.board[12], self.board[13], self.board[14], self.board[15],
            self.board[16], self.board[17], self.board[18], self.board[19],
            self.board[20], self.board[21], self.board[22], self.board[23],
            self.white_off_board_pieces
        )
