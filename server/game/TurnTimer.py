from threading import Timer, Thread, Event
from common.logger import logger


class TurnTimer(Thread):

    def __init__(self, game, event):
        logger.info("TurnTimer.__init__ turn timer created.")
        Thread.__init__(self)
        self.game = game
        self.turn_timer_event = event

    def run(self):
        while True:
            if self.turn_timer_event.wait(30.0):
                logger.info("TurnTimer.run player finished turn so next turn has been called.")
            else:
                logger.info("TurnTimer.run TIMEOUT!!! Next turn has been called.")
            self.game.dice_in_play.clear()
            logger.info("TurnTimer.run cleared dice in play: {}.".format(self.game.dice_in_play))
            self.game.next_turn()
            # Only iterate once.
            self.turn_timer_event.clear()
