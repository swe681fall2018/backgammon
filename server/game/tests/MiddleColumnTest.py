import unittest

from game.Game import Game

class MiddleColumnTest(unittest.TestCase):
    """ Tests moves when the user has a piece in the middle column of the
    board. Josh please ignore this. I'm just doing this to play around
    with Python's testing framework.
    """
    def setUp(self):
        """
        Setting up the following board:

        Black middle of board: ['B']
        White middle of board: ['W']

        Black pieces off the board: []
        Row 0: []
        Row 1: []
        Row 2: ['B']
        Row 3: []
        Row 4: []
        Row 5: []
        Row 6: []
        Row 7: []
        Row 8: []
        Row 9: []
        Row 10: []
        Row 11: []
        Row 12: []
        Row 13: []
        Row 14: []
        Row 15: []
        Row 16: []
        Row 17: []
        Row 18: []
        Row 19: []
        Row 20: []
        Row 21: []
        Row 22: []
        Row 23: ['W']
        White pieces off the board: []
        """
        self.game = Game()
        self.game.board = [[] for _ in range(24)]
        self.game.board[2].append(self.game.BLACK_PIECE)
        self.game.board[23].append(self.game.WHITE_PIECE)
        self.game.black_middle_board_pieces.append(self.game.BLACK_PIECE)
        self.game.white_middle_board_pieces.append(self.game.WHITE_PIECE)
        print(self.game)
        self.game.dice_in_play.extend([1, 3])
        self.game.current_players_turn = self.game.PLAYER_1  # black piece

    def test_black_move_to_empty_space(self):
        dst_column = 21
        self.game.move(self.game.MIDDLE_COLUMN, dst_column)
        self.assertEqual(self.game.board[dst_column], ['B'])

    def test_white_move_to_empty_space(self):
        """ Successfully move white piece to empty space.
        """
        self.game.current_players_turn = self.game.PLAYER_2
        dst_column = 0
        self.assertEqual(self.game.move(
            self.game.MIDDLE_COLUMN, dst_column), 1
            )

    def test_move_to_occupied_space_and_replace(self):
        """ Move the middle piece into a column that a single opponent's piece
        is on. It should replace the piece.
        """
        return_value = True
        dst_column = 23
        self.game.move(self.game.MIDDLE_COLUMN, dst_column)
        return_value &= len(self.game.white_middle_board_pieces) == 2
        return_value &= self.game.board[dst_column] == ['B']
        self.assertTrue(return_value)

    def test_deny_move_middle_column_not_empty(self):
        """ Denies a user's move because the middle column is NOT empty.
        """
        self.assertIsNone(self.game.move(2, 3))

    def test_deny_move_invalid_distance(self):
        """ Denies a user's move because the distance isn't in the current dice
        rolls.
        """
        dst_column = 22
        self.assertIsNone(self.game.move(self.game.MIDDLE_COLUMN, dst_column))
