import unittest

from game.Game import Game
from functools import wraps

def print_test_case(test_case):
    """ Annotation to print the test case name before it executes.
    """
    @wraps(test_case)
    def tmp(*args, **kwargs):
        print("="*15 + " " + test_case.__name__ + " " + "="*15)
        return test_case(*args, **kwargs)
    return tmp

class MoveTest(unittest.TestCase):
    """ Tests possible user moves.
    """
    def setUp(self):
        """
        Setting up the following board:

        Black middle of board: []
        White middle of board: []

        Black pieces off the board: []
        Row 0: ['W']
        Row 1: []
        Row 2: ['B']
        Row 3: []
        Row 4: []
        Row 5: []
        Row 6: ['W', 'W']
        Row 7: []
        Row 8: ['W']
        Row 9: ['B']
        Row 10: []
        Row 11: []
        Row 12: ['B', 'B']
        Row 13: []
        Row 14: []
        Row 15: []
        Row 16: []
        Row 17: []
        Row 18: []
        Row 19: ['B']
        Row 20: []
        Row 21: []
        Row 22: []
        Row 23: ['W']
        White pieces off the board: []
        """
        self.game = Game()
        self.game.board = [[] for _ in range(24)]
        self.game.board[0].append(self.game.WHITE_PIECE)
        self.game.board[2].append(self.game.BLACK_PIECE)
        self.game.board[6].extend([self.game.WHITE_PIECE, self.game.WHITE_PIECE])
        self.game.board[8].append(self.game.WHITE_PIECE)
        self.game.board[9].append(self.game.BLACK_PIECE)
        self.game.board[12].extend([self.game.BLACK_PIECE, self.game.BLACK_PIECE])
        self.game.board[19].append(self.game.BLACK_PIECE)
        self.game.board[23].append(self.game.WHITE_PIECE)
        print(self.game)
        self.game.dice_in_play.extend([1, 3])
        self.game.current_players_turn = self.game.PLAYER_1  # black piece

    @print_test_case
    def test_black_move_to_empty_space(self):
        """ Successfully moves to an empty space.
        """
        self.game.move(19, 18)
        self.assertEqual(self.game.board[18], [self.game.BLACK_PIECE])

    @print_test_case
    def test_white_move_to_empty_space(self):
        """ Successfully moves to an empty space.
        """
        self.game.current_players_turn = self.game.PLAYER_2
        self.game.move(6, 7)
        self.assertEqual(self.game.board[6], [self.game.WHITE_PIECE])

    @print_test_case
    def test_black_move_invalid_distance(self):
        """ Move rejected because distance is not in current dice values.
        """
        self.assertIsNone(self.game.move(19, 17))

    @print_test_case
    def test_white_move_invalid_distance(self):
        """ Move rejected because distance is not in current dice values.
        """
        self.game.current_players_turn = self.game.PLAYER_2
        self.assertIsNone(self.game.move(8, 10))

    @print_test_case
    def test_black_move_backwards(self):
        """ Attempt to move piece in the wrong direction a correct amount of
        distance.
        """
        self.assertIsNone(self.game.move(12, 13))

    @print_test_case
    def test_white_move_backwards(self):
        """ Attempt to move piece in the wrong direction a correct amount of
        distance.
        """
        self.assertIsNone(self.game.move(8, 7))

    @print_test_case
    def test_black_move_piece_off_board(self):
        """ Move pieces off of the board by the exact distance. E.g. if the
        piece is 4 spaces away from leaving the board, then it tries to move 4
        spaces.
        """
        # Clear the other columns so we can move a piece off of the board.
        self.game.board[9] = []
        self.game.board[12] = []
        self.game.board[19] = []
        return_value = True
        return_value &= self.game.move(2, self.game.OFF_BLACK) == 1
        return_value &= self.game.black_off_board_pieces == ["B"]
        print("Final output: " + str(self.game))
        self.assertTrue(return_value)

    @print_test_case
    def test_white_move_piece_off_board(self):
        """ Move pieces off of the board by the exact distance. E.g. if the
        piece is 4 spaces away from leaving the board, then it tries to move 4
        spaces.
        """
        self.game.current_players_turn = self.game.PLAYER_2
        # Clear the other columns so we can move a piece off of the board.
        self.game.board[0] = []
        self.game.board[6] = []
        self.game.board[8] = []
        return_value = True
        return_value &= self.game.move(23, self.game.OFF_WHITE) == 1
        return_value &= self.game.white_off_board_pieces == ["W"]
        print("Final output: " + str(self.game))
        self.assertTrue(return_value)

    @print_test_case
    def test_black_prevent_moving_piece_off_board(self):
        """ Prevent moving black piece off the board because there are other
        pieces around the board that need to be towards the end of the game
        board.
        """
        self.assertIsNone(self.game.move(2, self.game.OFF_BLACK))

    @print_test_case
    def test_white_prevent_moving_piece_off_board(self):
        """ Prevent moving black piece off the board because there are other
        pieces around the board that need to be towards the end of the game
        board.
        """
        self.game.current_players_turn = self.game.PLAYER_2
        self.assertIsNone(self.game.move(23, self.game.OFF_WHITE))

    @print_test_case
    def test_black_win(self):
        """ Player 1 should win the game.
        """
        self.game.board[9] = []
        self.game.board[12] = []
        self.game.board[19] = []
        # Must be 15 pieces off the board to win.
        self.game.black_off_board_pieces.extend([self.game.BLACK_PIECE]*14)
        print(self.game)
        self.assertEqual(self.game.move(2, self.game.OFF_BLACK), 3)

    @print_test_case
    def test_white_win(self):
        """ Player 2 should win the game.
        """
        self.game.current_players_turn = self.game.PLAYER_2
        self.game.board[0] = []
        self.game.board[6] = []
        self.game.board[8] = []
        # Must be 15 pieces off the board to win.
        self.game.white_off_board_pieces.extend([self.game.WHITE_PIECE]*14)
        print(self.game)
        self.assertEqual(self.game.move(23, self.game.OFF_WHITE), 3)
