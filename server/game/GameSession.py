from secrets import randbelow
import xml.etree.ElementTree as ET
from uuid import uuid4
from login.login import User
from common.mode import DEVELOPMENT_MODE
from common.logger import logger
from game.Game import Game
from http import cookies
from common.elastic_instance import elasticInstance

class GameSession:
    """ The liaison between XML requests and the game logic.
    """

    def __init__(self, player_1, player_2):
        self.player_1 = player_1
        self.player_2 = player_2
        self.game = Game()
        # TODO REMOVE
        """
        self.game.board[0] = []
        self.game.board[11] = []
        self.game.board[16] = []
        self.game.board[18] = []
        self.game.board[23] = ['W']
        self.game.white_off_board_pieces.extend([self.game.WHITE_PIECE]*14)
        """
        # TODO END REMOVE
        if DEVELOPMENT_MODE:
            self.game_uuid = 1
            self.player_1.uuid = '1'
            self.player_2.uuid = '2'
            """
            # Dummy test data to test win and lose logic. Places the white pieces
            # one move away from winning.
            self.game.board[0] = []
            self.game.board[11] = []
            self.game.board[16] = []
            self.game.board[18] = []
            self.game.board[23] = ['W']
            self.game.white_off_board_pieces.extend([self.game.WHITE_PIECE]*14)
            """
        else:
            # Strip "-" to avoid URL {en,de}coding.
            # Note: player's UUID is generated on login.
            self.game_uuid = uuid4().__str__().replace("-", "")
            logger.info("GameSession.__init__ game_uuid="+self.game_uuid)

        # Determines if win and loss has been updated in the respective
        # players database entry.
        self.game_outcome_stored_to_db = False

    @staticmethod
    def create(player_1, player_2):
        """ Gamesession builder. This was created to return UUIDs for the two players 
        along with the GameSession object whenever a new game session is created.
        """
        game = GameSession(player_1, player_2)
        return game, game.player_1.uuid, game.player_2.uuid

    def get_request_handler(self, backgammon_http_request_handler, tokenized_url):
        """ Handles GET requests that contain a path that matches a game's UUID. This
        function separates request types that require the player to have the current
        turn.
        """
        request_type = tokenized_url['request_type']
        if request_type == 'new_board':
            # This call is can return cookies so end_headers() cannot be called
            # immediately.
            self.new_board(backgammon_http_request_handler, tokenized_url)
            return
        
        backgammon_http_request_handler.end_headers()
        if request_type == 'initial_dice_roll':
            self.initial_dice_roll(backgammon_http_request_handler, tokenized_url)
            return
        elif request_type == 'get_board':
            self.get_board(backgammon_http_request_handler, tokenized_url)
            return
        elif request_type == 'poll':
            # If a player won, return the winning color piece.
            if self.game.player_who_won != None:
                backgammon_http_request_handler.wfile.write(self.game.player_who_won.encode('utf-8'))
                return
            # Nobody has one yet, so notify the user whos turn it is.
            is_users_turn = False
            if tokenized_url['player_uuid'] == self.player_1.uuid:
                if self.game.current_players_turn == self.game.PLAYER_1:
                    is_users_turn = True
            else:
                if self.game.current_players_turn == self.game.PLAYER_2:
                    is_users_turn = True
            if is_users_turn:
                backgammon_http_request_handler.wfile.write('Your turn!'.encode('utf-8'))
            else:
                backgammon_http_request_handler.wfile.write("Opponent's turn...".encode('utf-8'))
            return

        # ALL FUNCTIONS BELOW THIS REQUIRE THE CURRENT TURN TO EXECUTE!!!
        if tokenized_url['player_uuid'] == self.player_1.uuid and self.game.current_players_turn != self.game.PLAYER_1:
            logger.error(
                "GameSession.get_request_handler player 1 tried to take a "
                "turn when it wasn't there turn."
                )
            backgammon_http_request_handler.wfile.write("It's not your turn!".encode('utf-8'))
            return
        elif tokenized_url['player_uuid'] == self.player_2.uuid and self.game.current_players_turn != self.game.PLAYER_2:
            logger.error(
                "GameSession.get_request_handler player 2 tried to take a "
                "turn when it wasn't there turn."
                )
            backgammon_http_request_handler.wfile.write("It's not your turn!".encode('utf-8'))
            return

        if request_type == 'roll_dice':
            self.roll_dice(backgammon_http_request_handler, tokenized_url)
        elif request_type == 'move_piece':
            self.move_piece(backgammon_http_request_handler, tokenized_url)

    def new_board(self, backgammon_http_request_handler, tokenized_url):
        """ The request type is new_board. This function determines
        whether the client is asking for the html or javascript file. This
        is called when the client initially requests a game board.

        Assumptions:
        * UUID has already been verified.

        Example:
        /{game_uuid}?player_uuid={player_uuid}&request_type=new_board&content_type={html,js}
        """
        fd = None
        byte_stream_to_return = None
        if tokenized_url['content_type'] == "html":
            fd = open("../client/backgammon.html", "r")
            unformatted_html = fd.read()
            player_uuid = tokenized_url['player_uuid']
            opponent = None
            if player_uuid == self.player_1.uuid:
                opponent = self.player_2
            else:
                opponent = self.player_1
            # Insert the client's username, wins, losses, and opponent's name.
            player = self.get_player(player_uuid)
            user_info = "<p>Name: {} {} <br>Wins: {}<br>Losses: {}<br>Opponent's name: {}</p>".format(
                player.first_name, player.last_name, player.wins, player.losses, 
                opponent.first_name + " " + opponent.last_name
                )
            # Insert the client's UUID into the returned HTML.
            formatted_html = unformatted_html.format(
                self.game_uuid, player_uuid, user_info
                )
            byte_stream_to_return = formatted_html.encode('utf-8')
            backgammon_http_request_handler.send_secure_header()
        elif tokenized_url['content_type'] == "js":
            cookie = cookies.SimpleCookie()
            cookie['game_uuid'] = self.game_uuid
            cookie['new_player_uuid'] = tokenized_url['player_uuid'] # because the original player_uuid gets changed...
            print('new_board js game_uuid={} player_uuid={}'.format(self.game_uuid, tokenized_url['player_uuid']))
            print('P1: {} {} P2: {} {}'.format(self.player_1.first_name, self.player_1.uuid, self.player_2.first_name, self.player_2.uuid))
            backgammon_http_request_handler.send_header('Set-Cookie', cookie.output(header=''))
            backgammon_http_request_handler.send_header('Content-Type', 'text/javascript')
            backgammon_http_request_handler.send_header('Content-Security-Policy', "frame-ancestors 'none';")
            backgammon_http_request_handler.send_header('X-Frame-Options', "DENY")
            fd = open("../client/backgammon.js", "rb")
            byte_stream_to_return = fd.read()
        else:
            # Unknown path
            backgammon_http_request_handler.wfile.write(
                str.encode(backgammon_http_request_handler.responses[404][1]))
            logger.error("GameSession.return_game_board: 404 path was: " + 
                backgammon_http_request_handler.path)
            return
        backgammon_http_request_handler.end_headers()
        backgammon_http_request_handler.wfile.write(byte_stream_to_return)
        fd.close()

    def initial_dice_roll(self, backgammon_http_request_handler, tokenized_url):
        """ Determines which player goes first. This is called by each client 
        right after the game board is initially loaded. It also returns the color
        pieces that were assigned to the user.

        Assumptions:
        * UUID has already been sanitized and verified.

        <initial_dice_roll>
            <your_turn>{true, false}</your_turn>
            <color_piece>{B, W}</color_piece>
        <initial_dice_roll>
        """
        if self.game.current_players_turn == None:
            self.game.determine_who_goes_first()

        current_turn = None
        if tokenized_url['player_uuid'] == self.player_1.uuid:
            current_turn = self.game.PLAYER_1
        else:
            current_turn = self.game.PLAYER_2

        logger.info(
            "GameSession.initial_dice_roll current player={} game current={} "
            " p1 uuid={} passed in uuid={}.".format(
                current_turn, self.game.current_players_turn, self.player_1.uuid,
                tokenized_url['player_uuid']
                )
            )

        # Create and send XML response.
        init_dice_roll_root = ET.Element("initial_dice_roll")
        if self.game.current_players_turn == current_turn:
            ET.SubElement(init_dice_roll_root, "your_turn").text = 'true'
            logger.info("GameSession.initial_dice_roll your turn.")
        else:
            ET.SubElement(init_dice_roll_root, "your_turn").text = 'false'
            logger.info("GameSession.initial_dice_roll not your turn.")
        if tokenized_url['player_uuid'] == self.player_1.uuid:
            ET.SubElement(init_dice_roll_root, "color_piece").text = self.game.PLAYER_1
        else:
            ET.SubElement(init_dice_roll_root, "color_piece").text = self.game.PLAYER_2
        tree = ET.ElementTree(init_dice_roll_root)
        tree.write(backgammon_http_request_handler.wfile)

    def roll_dice(self, backgammon_http_request_handler, tokenized_url):
        """ Rolls two dice for the user and responds in XML format:

        <roll>
            <roll_1> </roll_1>
            <roll_2> </roll_2>
        </roll>

        The roll should be between [1,6].
        """
        if self.game.roll_dice() == False:
            logger.error("GameSession.roll_dice player already rolled! Returning current dice.")
        roll_root = ET.Element("roll")
        # Return all dice in play.
        if len(self.game.dice_in_play) >= 1:
            ET.SubElement(roll_root, "dice_1").text = str(self.game.dice_in_play[0])
        else:
            ET.SubElement(roll_root, "dice_1").text = str('')
        if len(self.game.dice_in_play) >= 2:
            ET.SubElement(roll_root, "dice_2").text = str(self.game.dice_in_play[1])
        else:
            ET.SubElement(roll_root, "dice_2").text = str('')
        if len(self.game.dice_in_play) >= 3:
            ET.SubElement(roll_root, "dice_3").text = str(self.game.dice_in_play[2])
        else:
            ET.SubElement(roll_root, "dice_3").text = str('')
        if len(self.game.dice_in_play) == 4:
            ET.SubElement(roll_root, "dice_4").text = str(self.game.dice_in_play[3])
        else:
            ET.SubElement(roll_root, "dice_4").text = str('')
        tree = ET.ElementTree(roll_root)
        tree.write(backgammon_http_request_handler.wfile)

    def get_board(self, backgammon_http_request_handler, tokenized_url):
        """ Returns the game board in a JSON array. The first 24 arrays are
        the rows of the board. The next two are black and white middle of
        the board pieces. The last two are black and white pieces off of the
        board.
        """
        json = """
        [
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {},
            {}
        ]
        """.format(
            self.game.board[0], self.game.board[1], self.game.board[2],
            self.game.board[3], self.game.board[4], self.game.board[5],
            self.game.board[6], self.game.board[7], self.game.board[8],
            self.game.board[9], self.game.board[10], self.game.board[11],
            self.game.board[12], self.game.board[13], self.game.board[14],
            self.game.board[15], self.game.board[16], self.game.board[17],
            self.game.board[18], self.game.board[19], self.game.board[20],
            self.game.board[21], self.game.board[22], self.game.board[23],
            self.game.black_middle_board_pieces, self.game.white_middle_board_pieces,
            self.game.black_off_board_pieces, self.game.white_off_board_pieces
        )
        logger.info("GameSession.get_board returning JSON " + json)
        backgammon_http_request_handler.wfile.write(json.encode('utf-8'))

    def move_piece(self, backgammon_http_request_handler, tokenized_url):
        """ Moves a piece on the game board.

        Return:
        * 1 - move accepted
        * 2 - next players turn
        * 3 - a player won the game (register it with the database)
        """
        # TODO(Tyler): sanitize the src_column and dst_column.
        if 'src_column' not in tokenized_url or 'dst_column' not in tokenized_url:
            logger.error(
                "GameSession.move_piece must contain src_column and dst_column"
                " parameters."
                )
            backgammon_http_request_handler.wfile.write(
                'Request must contain src_column and dst_column parameters.'
                )
        return_val = self.game.move(int(tokenized_url['src_column']), int(tokenized_url['dst_column']))
        if return_val == 1:
            backgammon_http_request_handler.wfile.write("1".encode('utf-8'))
        elif return_val == 2:
            backgammon_http_request_handler.wfile.write("2".encode('utf-8'))
        elif return_val == 3:
            # Return the player piece color that won.
            if len(self.game.black_off_board_pieces) == 15:
                backgammon_http_request_handler.wfile.write("B".encode('utf-8'))
                if self.game_outcome_stored_to_db == False:
                    elasticInstance.increment_wins(self.player_1.email)
                    elasticInstance.increment_losses(self.player_2.email)
                    self.player_1.wins += 1
                    self.player_2.losses += 1
                    self.game_outcome_stored_to_db = True
            else:
                backgammon_http_request_handler.wfile.write("W".encode('utf-8'))
                if self.game_outcome_stored_to_db == False:
                    elasticInstance.increment_losses(self.player_1.email)
                    elasticInstance.increment_wins(self.player_2.email)
                    self.player_1.losses += 1
                    self.player_2.wins += 1
                    self.game_outcome_stored_to_db = True
        else:
            logger.info("GameSession.move_piece returning invalid move to client.")
            backgammon_http_request_handler.wfile.write("Invalid move.".encode('utf-8'))

    def get_player(self, uuid):
        if uuid == self.player_1.uuid:
            return self.player_1
        elif uuid == self.player_2.uuid:
            return self.player_2
        else:
            logger.error("Game.get_player unknown uuid " + str(uuid))
            return None
