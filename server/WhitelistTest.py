import unittest
import sys

from whitelist import URLParamsWhitelist
from functools import wraps

def print_test_case(test_case):
    """ Annotation to print the test case name before it executes.
    """
    @wraps(test_case)
    def tmp(*args, **kwargs):
        print("="*15 + " " + test_case.__name__ + " " + "="*15)
        return test_case(*args, **kwargs)
    return tmp

class WhitelistTest(unittest.TestCase):
    def setUp(self):
        #sys.path.append('..') # Adds parent directory to Python path.
        self.whitelist = URLParamsWhitelist()
        self.tokenized_url = {
            'path' : '/123456789abcxyz',
            'player_uuid' : 'abcxyz1234567890',
            'request_type' : 'new_board',
            'content_type' : 'html'
        }

    @print_test_case
    def test_player_valid_game_uuid(self):
        self.assertTrue(self.whitelist.checkPair(self.tokenized_url))

    @print_test_case
    def test_valid_new_board(self):
        self.assertTrue(self.whitelist.checkPair(self.tokenized_url))

    @print_test_case
    def test_valid_initial_dice_roll(self):
        self.tokenized_url['request_type'] = 'initial_dice_roll'
        del self.tokenized_url['content_type']
        self.assertTrue(self.whitelist.checkPair(self.tokenized_url))

    @print_test_case
    def test_valid_move_piece(self):
        self.tokenized_url['request_type'] = 'move_piece'
        del self.tokenized_url['content_type']
        all_permutations_passed = True
        for i in range(-2, 25):
            for j in range(-2, 25):
                self.tokenized_url['src_column'] = str(i)
                self.tokenized_url['dst_column'] = str(j)
                all_permutations_passed &= self.whitelist.checkPair(self.tokenized_url)
                # Remove comment below to debug test case.
                #print("i={} j={} returned={}".format(i, j, all_permutations_passed))
        self.assertTrue(all_permutations_passed)

    @print_test_case
    def test_player_game_uuid_invalid_game_uuid_non_alphanum(self):
        self.tokenized_url['path'] += '-'
        self.assertFalse(
            self.whitelist.check_game_and_player_uuids(self.tokenized_url)
            )

    @print_test_case
    def test_new_board_invalid_content_type(self):
        self.tokenized_url['content_type'] = 'htmll'
        self.assertFalse(self.whitelist.checkPair(self.tokenized_url))

    @print_test_case
    def test_new_board_invalid_content_type_non_alphanum(self):
        self.tokenized_url['content_type'] = 'html-'
        self.assertFalse(self.whitelist.checkPair(self.tokenized_url))

    @print_test_case
    def test_new_board_invalid_content_type_multiple_valid_combined(self):
        self.tokenized_url['content_type'] = 'htmljs'
        self.assertFalse(self.whitelist.checkPair(self.tokenized_url))

    @print_test_case
    def test_move_piece_high_bound(self):
        self.tokenized_url['request_type'] = 'move_piece'
        del self.tokenized_url['content_type']
        a_single_permutation_passed = False
        self.tokenized_url['src_column'] = '0'
        for i in range(25, 100):
            self.tokenized_url['dst_column'] = str(i)
            a_single_permutation_passed |= self.whitelist.checkPair(self.tokenized_url)
        self.assertFalse(a_single_permutation_passed)

    @print_test_case
    def test_move_piece_low_bound(self):
        self.tokenized_url['request_type'] = 'move_piece'
        del self.tokenized_url['content_type']
        a_single_permutation_passed = False
        self.tokenized_url['dst_column'] = '0'
        for i in range(-3, -35):
            self.tokenized_url['src_column'] = str(i)
            a_single_permutation_passed |= self.whitelist.checkPair(self.tokenized_url)
        self.assertFalse(a_single_permutation_passed)

    @print_test_case
    def test_move_piece_non_numeric(self):
        self.tokenized_url['request_type'] = 'move_piece'
        del self.tokenized_url['content_type']
        a_single_permutation_passed = False
        self.tokenized_url['dst_column'] = '0'
        for i in ['-', 'a', '?', 'A', '`', '12a3', '-1a', 'a1']:
            self.tokenized_url['src_column'] = str(i)
            a_single_permutation_passed |= self.whitelist.checkPair(self.tokenized_url)
        self.assertFalse(a_single_permutation_passed)