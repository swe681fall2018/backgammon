from http.server import HTTPServer, BaseHTTPRequestHandler
from http import cookies
from ssl import wrap_socket
from io import BytesIO
from whitelist import URLWhiteList, URLParamsWhitelist
from elastic import Elastic
from sys import path
from os import getcwd
import hashlib

from login.login import login_url_path_handler
from login.login import do_login
from login.login import User
from common.logger import logger
from common.mode import DEBUG, DEVELOPMENT_MODE
from match_making.MatchMakingQueue import MatchMakingQueue
from game.GameSession import GameSession
from common.elastic_instance import elasticInstance

# Handles all URLs that are not related to a current game session.
url_path_handlers = {
    "login_url_handlers" : ["/login.html", "/login", "/", ""],
    "find_game_handlers" : ["/find_game"],
    "get_player_records_handlers" : ["/get_player_records"]
}

# A map of all current UUIDs -> game sessions.
current_games = {}

# map of uuid to person
loginMap = dict()

#Globals for login
whitelist = URLWhiteList()
url_params_whitelist = URLParamsWhitelist()
matchQueue = MatchMakingQueue()

class BackgammonHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        global matchQueue
        global loginMap
        logger.info('Path is ' + self.path)

        name_value_pairs = self.tokenize_url_parameters()
        path = name_value_pairs['path']
        if path in url_path_handlers['login_url_handlers']:
            # Handles its own sanitization with hard coded checks.
            login_url_path_handler(self)
            return

        # Sanitize the tokenized URL name value pairs.
        if url_params_whitelist.checkPair(name_value_pairs) == False:
            self.send_secure_header()
            return self.fail_response(400, 'Invalid data sent to server.')
        
        if path in url_path_handlers['find_game_handlers']:
            self.find_game_request_handler(name_value_pairs)
        elif path in url_path_handlers['get_player_records_handlers']:
            self.get_player_records_handler(name_value_pairs)
        else:
            # Request related to current game session.
            # /<game session uuid>?<player uuid>={}&request_type={}
            if 'player_uuid' not in name_value_pairs.keys():
                logger.error(
                    "BackgammonHTTPRequestHander.do_GET player_uuid not "
                    "specified in URL."
                    )
                return

            # Ensure the player provided a valid player_uuid.
            player_uuid = name_value_pairs['player_uuid']
            if player_uuid not in current_games:
                logger.error(
                    "BackgammonHTTPRequestHandler.do_GET player_uuid not "
                    "in list of current games."
                    )
                # Don't give user detailed message.
                self.return_response_code(404, "")
                return

            # Send OK plus game and user UUIDs then handle HTML response.
            self.send_response(200)
            current_games[player_uuid].get_request_handler(
                self, name_value_pairs
                )

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        body = body.decode("utf-8")
        #logger.info("POST recv: " + body)
        params = body.split('&')
        d = dict(inner.split('=') for inner in params)

        if ('fname' in d) or ('email' in d):
            # Do the actual login this is a login request
            print('IN POST DO LOGIN')
            loggedIn, user = do_login(body, elasticInstance, loginMap)

            print('Login is ' + str(loggedIn))
            if loggedIn == False:
                return
            print('uuid ' + str(user.uuid))
            key = str(user.uuid)
            print ('At key ' + str(loginMap[key].email))
            fd = open("../client/pregame_lobby.html", "rb")

            self.send_response(200)
            cookie = cookies.SimpleCookie()
            cookie['player_uuid'] = key
            self.send_header('Set-Cookie', cookie.output(header=''))
            self.send_secure_header()
            self.end_headers()

            self.wfile.write(fd.read())
            fd.close()

        elif self.path in url_path_handlers:
            url_path_handlers[self.path](self)

        else:
            self.send_response(200)
            self.send_secure_header()
            self.end_headers()

    def send_secure_header(self):
        # Prevent reducing MIME type attacks.
        self.send_header('Content-Type', 'text/html')
        self.send_header('X-Content-Type-Options', 'nosniff')
        # Prevent click jacking.
        self.send_header('Content-Security-Policy', "frame-ancestors 'none';")
        self.send_header('X-Frame-Options', "DENY")

    def find_game_request_handler(self, name_value_pairs):
        self.send_response(200)
        self.send_secure_header()
        self.end_headers()
        # Verify that player is logged in.
        if name_value_pairs['player_uuid'] not in loginMap:
            print("find_game_request_handler player_uuid not in loginMap.")
            self.wfile.write("False".encode('utf-8'))
            return

        # Return game UUID if the user is part of a current game.
        if name_value_pairs['player_uuid'] in current_games:
            game_uuid = current_games[name_value_pairs['player_uuid']].game_uuid
            self.wfile.write(str(game_uuid).encode('utf-8'))
            return

        # Add player to the current game queue.
        player = loginMap[name_value_pairs['player_uuid']]
        opponent = matchQueue.find_opponent(player)
        if opponent != None:
            gameSession, uuid1, uuid2 = GameSession.create(player, opponent)
            current_games[uuid1] = gameSession
            current_games[uuid2] = gameSession
            self.wfile.write(str(gameSession.game_uuid).encode('utf-8'))
        else:
            self.wfile.write("False".encode('utf-8'))

    def get_player_records_handler(self, tokenized_url):
        self.send_response(200)
        self.send_secure_header()
        self.end_headers()
        if tokenized_url['player_uuid'] not in loginMap:
            logger.error("get_player_records_handler user must be authenticated to view player records!")
            self.wfile.write("".encode('utf-8'))
            return
        # Return the formatted player records.
        player_records = elasticInstance.query_all()
        logger.info("get_player_records_handler " + str(player_records))
        logger.info("loginmap " + str(loginMap.keys()))
        response = ""
        for player_record in player_records:
            response += "User {} {}<br>Wins: {}<br>Losses: {}<br><br>".format(
                player_record[0], player_record[1], player_record[2], player_record[3]
            )
        self.wfile.write(response.encode('utf-8'))

    def fail_response(self, code, fail_msg):
        """ Return failure messages to the client.
        """
        self.send_response(code)
        self.end_headers()
        self.wfile.write(str.encode(fail_msg))
        logger.info("BackgammonHTTPRequestHandler.fail_response " + fail_msg)

    def tokenize_url_parameters(self):
        """ Parses URL parameters and creates a dictionary of name-value 
        pairs.
        
        Example: /backgammon.html?uuid=1234&request_type=new_board ->
        {'path' : '/backgammon', 'uuid' : 1234, 'request_type' : 'new_board'}
        """
        # TODO(Tyler) sanitize with regex.
        if '?' in self.path:
            path, params_only = self.path.split('?')
        else:
            return {'path' : self.path}
        name_value_pairs_dict = {}
        name_value_pairs_dict['path'] = path
        name_value_pairs = params_only.split('&')
        for name_value_pair in name_value_pairs:
            name, value = name_value_pair.split('=')
            name_value_pairs_dict[name] = value
        logger.info("Server.tokenize_url_parameters " + name_value_pairs_dict.__str__())
        return name_value_pairs_dict

    def return_response_code(self, code, msg):
        self.send_response(code) # e.g. 404
        self.end_headers()
        self.wfile.write(str.encode(msg))
        logger.info("Returning error code " + str(code) + " with msg: " + msg)

# TODO(Tyler): remove later.
def dev_game_handler(BackgammonHTTPRequestHandler):
    if not DEVELOPMENT_MODE:
        return
    # Would normally parse the GET or POST for UUID and call the respective
    # game handler. Since this is a developer mode with hard coded UUIDs, 
    # we can skip this step and just call the handler.
    current_games["1"].game_get_request_handler(BackgammonHTTPRequestHandler)

def main():
    # Add server to PYTHONPATH environment variable so submodules can access
    # debug variable.
    path.append(getcwd())

    # TODO: use logging system and sanitize log input.
    httpd = HTTPServer(('localhost', 4443), BackgammonHTTPRequestHandler)
    httpd.socket = wrap_socket(
        httpd.socket, 
        # To create the development certs in the dev_certs directory:
        # openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365
        keyfile="dev_certs/key.pem", 
        certfile='dev_certs/cert.pem', 
        server_side=True
    )
    if DEVELOPMENT_MODE:
        # Create a fake game with two users.
        player_1 = User("p1@gmail.com", "player", "one", "69", "420")
        player_2 = User("p2@gmail.com", "player", "two", "123", "9001")
        game_, player_1_uuid, player_2_uuid = GameSession.create(player_1, player_2)
        current_games[player_1_uuid] = game_
        current_games[player_2_uuid] = game_
    print(locals())
    httpd.serve_forever()

if __name__ == "__main__":
    main()
