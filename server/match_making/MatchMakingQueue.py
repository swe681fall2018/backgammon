class MatchMakingQueue():

    def __init__(self):
        self.player_waiting_for_opponent = None

    def find_opponent(self, player):
        """ Returns any players waiting for an opponent. If no players are
        waiting for an opponent, the requesting player starts waiting.

        Return:
        * opponent - the player waiting for an opponent
        * None - no opponent found
        """
        if self.player_waiting_for_opponent == None:
            print('Player {} just started waiting for an opponent.'.format(player.uuid)) 
            self.player_waiting_for_opponent = player
            return None
        elif player == self.player_waiting_for_opponent:
            print('Player {} is still waiting for an opponent...'.format(player.uuid)) 
            return None
        else:
            print('Player {} found opponent {}! Starting game.'.format(
                player.uuid, self.player_waiting_for_opponent.uuid)
                )
            opponent = self.player_waiting_for_opponent
            self.player_waiting_for_opponent = None
            return opponent
