# Includes or excludes certain hard coded variables used during development.
DEVELOPMENT_MODE = False

# Determines the verbosity of the logging.
DEBUG = True

def print_d(message):
    if DEBUG:
        print("[DEBUG] " + message)