import logging
import re

from common.mode import print_d


class BackgammonLoggingFilter(logging.Filter):
    def filter(self, record):
        ''' Log white list to prevent log fogery. '''
        if re.match("[A-Za-z0-9!<>\\?,\\.\\&%\\^\\$\\#@]", record.getMessage()) != None:
            return True
        else:
            print_d("Logger.filter record {} was rejected by the filter.".format(record))
            return False