import logging

#from common.BackgammonLoggingFilter import BackgammonLoggingFilter
from common.BackgammonLoggingFilter import BackgammonLoggingFilter

# Create logger and use custom filter.
logger = logging.getLogger("backgammon_logger")
logger.setLevel(logging.INFO)
backgammon_filter = BackgammonLoggingFilter()
logger.addFilter(backgammon_filter)

# Log to stderr.
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)

# Log to backgammon.log.
file_handler = logging.FileHandler("backgammon.log")
file_handler.setLevel(logging.INFO)

# Add datetime and log level to log message.
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
stream_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

# Log to both stderr and backgammon.log when logging.
logger.addHandler(stream_handler)
logger.addHandler(file_handler)
