import re
from common.logger import logger


class URLWhiteList:
    def __init__(self):
        self.regexList = []
        # add all of the allowed elements here statically
        # this regex is for validating the sign up queries
        self.regexList.append('/login\?fname=[a-zA-Z\']{2,20}&lname=[a-zA-Z\'\.]{2,20}&email=[0-9a-zA-Z\'\!#\$\*\&\(\)\-\_]{2,20}%40[0-9a-zA-Z@\']{2,20}\.[0-9a-zA-Z@\']{2,10}&pw=[0-9a-zA-Z@\'\.]{10,20}')
        self.regexList.append('/\?fname=[a-zA-Z\']{2,20}&lname=[a-zA-Z\'\.]{2,20}&email=[0-9a-zA-Z\'\!#\$\*\&\(\)\-\_]{2,20}%40[0-9a-zA-Z@\']{2,20}\.[0-9a-zA-Z@\']{2,10}&pw=[0-9a-zA-Z@\'\.]{10,20}')
        #This regex validates login strings
        self.regexList.append('/login?\?email=[0-9a-zA-Z\'\!#\$\*\&\(\)\-\_]{2,20}%40[0-9a-zA-Z@\']{2,20}\.[0-9a-zA-Z@\']{2,10}&pw=[0-9a-zA-Z@\'\.]{10,20}')
        self.regexList.append('/\?email=[0-9a-zA-Z\'\!#\$\*\&\(\)\-\_]{2,20}%40[0-9a-zA-Z@\']{2,20}\.[0-9a-zA-Z@\']{2,10}&pw=[0-9a-zA-Z@\'\.]{10,20}')

    def checkValue(self, value):
        for ele in self.regexList:
            if re.match(ele, value):
                return True
        return False

class LoginWhiteList:
    def __init__(self):
        self.regexList = []
        # allowed usernames
        self.regexList.append('[0-9a-zA-Z\'\!#\$\*\&\(\)\-\_]{2,20}%40[0-9a-zA-Z@\']{2,20}\.[0-9a-zA-Z@\']{2,10}')
        # allowed passwords
        self.regexList.append('[0-9a-zA-Z@\'\!\#\$\%\^\&\*\(\)\=\+\-\_\~\`\>\<]{10,20}')

class URLParamsWhitelist:
    """ Whitelists for tokenized URL parameters.
    """

    def __init__(self):
        self.path_handler = {
            'new_board' : self.new_board,
            'initial_dice_roll' : self.check_game_and_player_uuids,
            'get_board' : self.check_game_and_player_uuids,
            'poll' : self.check_game_and_player_uuids,
            'roll_dice' : self.check_game_and_player_uuids,
            'move_piece' : self.move_piece
        }

    def checkPair(self, tokenized_url):
        """ Checks if the tokenized_url is formatted correctly based on the
        give url path.

        Return:
        * True - all valid inputs
        * False - contains invalid input
        """
        if tokenized_url['path'] == '/find_game':
            return self.find_game(tokenized_url)
        elif tokenized_url['path'] == '/get_player_records':
            return self.get_player_records(tokenized_url)

        if 'request_type' not in tokenized_url:
            logger.error("URLParamsWhiteList.checkPair request type not in tokenized urls.")
            return False
        request_type = tokenized_url['request_type']
        if request_type not in self.path_handler:
            logger.error(
                "URLParamsWhiteList.checkPair tokenized url path {} not in "
                "path handler.".format(request_type)
                )
        if self.path_handler[request_type](tokenized_url):
            logger.info("Tokenized URL ALLOWED: {}".format(tokenized_url))
            return True
        else:
            logger.error("Tokenized URL REJECTED: {}".format(tokenized_url))
            return False

    def check_game_and_player_uuids(self, tokenized_url):
        """ Checks that the game and player uuids are in a given in lowercase
        alphanum characters. Note that the path is the game uuid.

        Return: True or False
        """
        if 'player_uuid' not in tokenized_url or \
        'path' not in tokenized_url:
            logger.error(
                "URLParamsWhiteList.check_game_and_player_uuids player or game "
                "uuid not in tokenized url."
                )
            return False
        if re.match('^[a-z0-9]+$', tokenized_url['player_uuid']) == None:
            logger.error(
                "URLParamsWhiteList.check_game_and_player_uuids invalid "
                "player_uuid."
                )
            return False
        if re.match('^/[a-z0-9]+$', tokenized_url['path']) == None:
            logger.error(
                "URLParamsWhiteList.check_game_and_player_uuids invalid "
                "game_uuid (stored in path)."
                )
            return False
        return True

    def find_game(self, tokenized_url):
        """ Just verify that the player UUID is valid.
        """
        if re.match('^[a-z0-9]+$', tokenized_url['player_uuid']) == None:
            logger.error("URLParamsWhiteList.find_game invalid player_uuid.")
            return False
        return True

    def get_player_records(self, tokenized_url):
        """ Just verify that the player UUID is valid.
        """
        if re.match('^[a-z0-9]+$', tokenized_url['player_uuid']) == None:
            logger.error("URLParamsWhiteList.get_player_records invalid player_uuid.")
            return False
        return True

    def new_board(self, tokenized_url):
        if 'content_type' not in tokenized_url:
            logger.error(
                "URLParamsWhiteList.new_board content_type is not in tokenized url."
                )
            return False
        if re.match('(^html$|^js$)', tokenized_url['content_type']) == None:
            logger.error(
                "URLParamsWhiteList.new_board content_type is not html or js."
                )
            return False
        return self.check_game_and_player_uuids(tokenized_url)

    def move_piece(self, tokenized_url):
        """ Ensure that source and destination params are numeric [-2, 24].
        """
        if 'src_column' not in tokenized_url or 'dst_column' not in tokenized_url:
            logger.error(
                "URLParamsWhiteList.move_piece must contain src_column and dst_column"
                " parameters."
                )
            return False
        pattern = re.compile('^-[1,2]$|^1?[0-9]$|^2[0-4]$')
        if (pattern.match(tokenized_url['src_column']) and pattern.match(tokenized_url['dst_column'])) == None:
            logger.error(
                "URLParamsWhiteList.move_piece invalid source or destination parameter."
                )
            return False
        return self.check_game_and_player_uuids(tokenized_url) 
