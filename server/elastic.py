from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
import uuid

class Elastic:
    def __init__(self):
        ES_HOST = {"host": "localhost", "port": 9200}
        self.es = Elasticsearch(hosts=[ES_HOST])
        INDEX_NAME = 'new'
        self.id = 1
        if not self.es.indices.exists(index=INDEX_NAME):
            response = self.es.indices.create(index=INDEX_NAME)
            print('Elastic search index creation return ' + str(response))


    def search(self, username):
        results = self.es.search(index="new", doc_type="users", body={"query": {"match_phrase": {"email": username}}})
        return results

    def insert(self, email, fname, lname, password):
        # generate a new uuid
        doc={'email' : email,
             'fname' : fname,
             'lname' : lname,
             'pw' : password,
             'wins': 0,
             'losses' : 0}

        self.es.index(index="new", doc_type="users", id=self.id, body=doc)
        self.id = self.id + 1
        self.es.indices.refresh(index="new")

    def query_all(self):
        es_response = scan(
            self.es,
            index='new',
            doc_type='users',
            query={"query": {"match_all": {}}}
        )

        # Return list of names and scores
        list_ = []
        for i in es_response:
            fname = (i['_source']['fname'])
            lname = (i['_source']['lname'])
            wins = (i['_source']['wins'])
            losses = (i['_source']['losses'])
            list_.append((fname, lname, wins, losses))
        return list_

    def increment_wins(self, username):
        results = self.search(username)
        print("Results: " + str(results))
        wins = results['hits']['hits'][0]['_source']['wins']
        wins = wins + 1
        print('Results are ' + str(results['hits']['hits'][0]['_id']))
        id = results['hits']['hits'][0]['_id']

        doc = {'email': results['hits']['hits'][0]['_source']['email'],
               'fname': results['hits']['hits'][0]['_source']['fname'],
               'lname': results['hits']['hits'][0]['_source']['lname'],
               'pw': results['hits']['hits'][0]['_source']['pw'],
               'wins': wins,
               'losses': results['hits']['hits'][0]['_source']['losses']}
        self.es.delete(index="new",doc_type="users",id=results['hits']['hits'][0]['_id'])
        self.es.index(index="new", doc_type="users", id=self.id, body=doc)
        self.id = self.id + 1
        self.es.indices.refresh(index="new")

        results = self.search(username)
        print('Update wins is ' + str(results['hits']['hits'][0]['_source']['wins']))

    def increment_losses(self, username):
        results = self.search(username)
        losses = results['hits']['hits'][0]['_source']['losses']
        losses = losses + 1
        id = results['hits']['hits'][0]['_id']

        doc = {'email': results['hits']['hits'][0]['_source']['email'],
               'fname': results['hits']['hits'][0]['_source']['fname'],
               'lname': results['hits']['hits'][0]['_source']['lname'],
               'pw': results['hits']['hits'][0]['_source']['pw'],
               'wins': results['hits']['hits'][0]['_source']['wins'],
               'losses': losses}

        self.es.delete(index="new",doc_type="users",id=results['hits']['hits'][0]['_id'])
        self.es.index(index="new", doc_type="users", id=self.id, body=doc)
        self.id = self.id + 1
        self.es.indices.refresh(index="new")

        results = self.search(username)