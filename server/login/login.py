# need to install es data base https://www.elastic.co/guide/en/elasticsearch/reference/current/_installation.html
# and the pip elastic search module
from uuid import uuid4
import hashlib
import re

class User:
    def __init__(self, email, first_name, last_name, wins, losses):
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.wins = wins
        self.losses = losses
        self.uuid = uuid4().__str__().replace("-", "") # Generated at the start of each game.

def escape_string(string):
    return re.escape(string)

def login_url_path_handler(backgammon_http_request_handler):
    backgammon_http_request_handler.send_response(200)
    backgammon_http_request_handler.send_secure_header()
    backgammon_http_request_handler.end_headers()
    fd = None
    path = backgammon_http_request_handler.path
    if path == "/login.html" or path == "/login" or path == "/" or path == "":
        fd = open("../client/login.html", "rb")
    elif path == "/login.css":
        fd = open("../client/login.css", "rb")
    else:
        # Unknown path
        backgammon_http_request_handler.wfile.write(
            str.encode(backgammon_http_request_handler.responses[404][1])
            )
        print("Login 404 path was: " + path)
        return

    backgammon_http_request_handler.wfile.write(fd.read())
    fd.close()


def hash_password(password):
    # uuid is used to generate a random number
    salt = uuid4().hex
    return hashlib.sha3_512(salt.encode() + password.encode()).hexdigest() + ':' + salt


def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    pw = hashlib.sha3_512(salt.encode() + user_password.encode()).hexdigest()
    print('pw is ' + pw + ' and the recovered pw is ' + str(password))
    return password == pw

def do_login(path, elastic, loginMap):
    """  split out the string. If it has a first and last name
         we will treat it as a sign up otherwise we try to login
         with the email and password."""
    # Remove bad characters here
    path = path.replace("/", "")
    path = path.replace("?", "")
    path = path.replace("\'", "")

    elements = path.split('&')
    d = dict(inner.split('=') for inner in elements)

    # strip bad characters
    for key, value in d.items():
        # escape the user input
        d[key] = escape_string(d[key])

    if 'fname' in d and 'lname' in d:
        # This is a sign up request
        result = elastic.search(d['email'])
        if result['hits']['total'] == 0:
            print('Inserting user into the database')
            # No user insert a new one
            elastic.insert(d['email'], d['fname'], d['lname'], hash_password(d['pw']))
            return login(path, elastic, loginMap, d)
        else:
            print('User already exists')
            return login(path, elastic, loginMap, d)
    else:
        return login(path, elastic, loginMap, d)

def login(path, elastic, loginMap, d):
    # try to login with email and pass
    result = elastic.search(d['email'])
    print('login results ' + str(result))
    if result['hits']['total'] == 1:
        if check_password(result['hits']['hits'][0]['_source']['pw'], d['pw']):
            # TODO(Tyler): return User object.
            result['hits']['hits'][0]['_source']['pw']

            fname = result['hits']['hits'][0]['_source']['fname']
            lname = result['hits']['hits'][0]['_source']['lname']
            wins = result['hits']['hits'][0]['_source']['wins']
            losses = result['hits']['hits'][0]['_source']['losses']
            email = result['hits']['hits'][0]['_source']['email']

            for uuid, player in loginMap.items():
                # player is logged in already
                if player.email == email:
                    return False, None

            u = User(email, fname, lname, wins, losses)
            loginMap[u.uuid] = u
            print('Success! Added {} to loginMap {}.'.format(u.uuid, loginMap))
            return True, u
    return False, None