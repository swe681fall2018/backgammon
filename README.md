# Backgammon
Tyler Lambert and Joshua Lilly's SWE-681 Fall 2018 Backgammon course project.  

# Elasticsearch Setup

1) Download from https://www.elastic.co/downloads/elasticsearch  
2) Untar it `tar -xf elasticsearch-6.5.3.tar.gz`  
3) Run `pip install elasticsearch`  
4) Run `./elasticsearch-6.5.3/bin/elasticsearch`  

# Server Install and Setup
```
$ mkdir ./server/dev_certs
$ openssl req -x509 -newkey rsa:2048 -keyout ./server/dev_certs/key.pem -out ./server/dev_certs/cert.pem -days 365
$ cd ./server
$ python server.py
```

# Dependencies
- Python 3
- Elasticsearch server (from https://www.elastic.co/guide/en/elasticsearch/reference/current/_installation.html)
- Python module from pip
      